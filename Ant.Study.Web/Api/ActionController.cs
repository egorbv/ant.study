﻿using Ant.Study.Common.Actions;
using Ant.Study.Common.Brokers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ant.Study.Web.Api
{
	[Route("api/[controller]")]
	[ApiController]
	public class ActionController : ControllerBase
	{
		IActionBroker _actionBroker;

		public ActionController(IActionBroker actionBroker)
		{
			_actionBroker = actionBroker;
		}

		[HttpPost]
		public IActionResult Post([FromQuery] string actionName, [FromForm] string data)
		{
			IActionArgs actionArgs = new BaseActionArgs();
			try
			{
				actionArgs.UserID = 10;
				actionArgs.Model = data;
				_actionBroker.Execute(actionName, actionArgs);
				return Ok(actionArgs.ReturnedModel);
			}
			catch (Exception e)
			{

			}

			return Ok();
		}

	}
}
