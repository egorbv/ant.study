﻿using Ant.Study.Common.Brokers;
using Ant.Study.Common.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ant.Study.Web.Api
{
	[Route("api/[controller]")]
	[ApiController]
	public class QueryController : ControllerBase
	{
		IQueryBroker _queryBroker;

		public QueryController(IQueryBroker queryBroker)
		{
			_queryBroker = queryBroker;
		}

		// GET: api/<QueryController>
		[HttpGet]
		public IActionResult Get(string queryName)
		{
			IQueryArgs queryArgs = new BaseQueryArgs();
			try
			{
				queryArgs.UserID = 10;
				_queryBroker.Request(queryName, queryArgs);
				return Ok(queryArgs.ReturnModel);
			}
			catch(Exception e)
			{

			}

			return Ok();
		}
	}
}
