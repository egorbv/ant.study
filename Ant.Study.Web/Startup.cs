using Ant.Study.Common.Brokers;
using Ant.Study.Common.Data;
using Ant.Study.CQRS;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Ant.Study.Web
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<IQueryBroker, QueryBroker>();
			services.AddSingleton<IActionBroker, ActionBroker>();
			StartupService.RegisterServices(services);

			services.AddControllersWithViews();
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                                  builder =>
                                  {
                                      builder.AllowAnyOrigin();
                                  });
            });

			var s = Configuration.GetConnectionString("DefaultConnectionString");
			StoreConnectionType.SetDefaultConnectionString(Configuration.GetConnectionString("DefaultConnectionString"));
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			var serviceScopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
			var serviceProvider = serviceScopeFactory.CreateScope().ServiceProvider;
			var actionBroker = serviceProvider.GetRequiredService<IActionBroker>();
			var queryBroker = serviceProvider.GetRequiredService<IQueryBroker>();

			StartupService.RegisterHandlers(actionBroker, queryBroker);


			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();

			app.UseRouting();
            app.UseCors();
            app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller}/{action=Index}/{id?}");
			});

        }
	}
}
