using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ant.Study.Web.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class WeatherForecastController : ControllerBase
	{
		private static readonly string[] Summaries = new[]
		{
			"Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
		};

		private readonly ILogger<WeatherForecastController> _logger;

		public WeatherForecastController(ILogger<WeatherForecastController> logger)
		{
			_logger = logger;
		}

		[HttpGet]
		public IEnumerable<ImageSource> Get()
		{
            var result = new List<ImageSource>();
            result.Add(new ImageSource() { Url = "http://localhost:44305/assets/images/", Images = new List<ImageInfo>() });
            result[0].Images.Add(new ImageInfo() { Name = "rrr1", PartOfUrl = "abuelo_abuela.png" });
            result[0].Images.Add(new ImageInfo() { Name = "rrr2", PartOfUrl = "abuelo_abuela1.png" });
            result[0].Images.Add(new ImageInfo() { Name = "rrr2", PartOfUrl = "abuelo_abuela2.png" });
            result.Add(new ImageSource() { Url = "http://localhost:44305/assets/images/", Images = new List<ImageInfo>() });
            result[1].Images.Add(new ImageInfo() { Name = "rrr3", PartOfUrl = "abuelo_abuela.png" });
            result[1].Images.Add(new ImageInfo() { Name = "rrr4", PartOfUrl = "abuelo_abuela1.png" });
            return result;
        }
    }


    public class ImageSource
    {
        public string Url { get; set; }
        public  List<ImageInfo> Images { get; set; }
    }

    public class ImageInfo
    {
        public string Name { get; set; }
        public string PartOfUrl { get; set; }
    }
}
