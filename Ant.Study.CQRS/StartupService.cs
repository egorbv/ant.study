﻿using Ant.Study.Common.Brokers;
using Ant.Study.CQRS.Actions.SaveScene;
using Ant.Study.CQRS.Queries.WordImages;
using Microsoft.Extensions.DependencyInjection;

namespace Ant.Study.CQRS
{
	public static class StartupService
	{
		/// <summary>
		/// Регистрация сервисов терминала
		/// </summary>
		/// <param name="serviceCollection">Экземпляр коллекции внедренных сервисов</param>
		public static void RegisterServices(IServiceCollection serviceCollection)
		{
			serviceCollection.AddSingleton<IWordImagesQuery, WordImagesQuery>();

			serviceCollection.AddSingleton<ISaveSceneAction, SaveSceneAction>();
		}

		/// <summary>
		/// Регистрация обработчиков терминала
		/// </summary>
		/// <param name="actionBroker">Экземпляр брокера экшенов</param>
		/// <param name="queryBroker">Экземпляр брокера запросов</param>
		public static void RegisterHandlers(IActionBroker actionBroker, IQueryBroker queryBroker)
		{ 
			queryBroker.Register<IWordImagesQuery>();

			actionBroker.Register<ISaveSceneAction>();
		}
	}
}
