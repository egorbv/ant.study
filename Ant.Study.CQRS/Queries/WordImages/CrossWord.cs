﻿using System.Text.Json.Serialization;

namespace Ant.Study.CQRS.Queries.WordImages
{
	public class CrossWord
	{
		[JsonPropertyName("msd")]
		public long MediaSourceItemID { get; set; }

		[JsonPropertyName("wi")]
		public long WordID { get; set; }

		[JsonPropertyName("n")]
		public string Name { get; set; }
	}
}
