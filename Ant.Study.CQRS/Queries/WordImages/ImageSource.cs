﻿
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Ant.Study.CQRS.Queries.WordImages
{
	public class ImageSource
	{
		[JsonPropertyName("i")]
		public long ID { get; set; }

		[JsonPropertyName("u")]
		public string Url { get; set; }

		[JsonPropertyName("it")]
		public List<ImageSourceItem> Items { get; set; }

		[JsonPropertyName("iow")]
		public List<CrossWord> IntersectionOfWords { get; set; }
	}
}
