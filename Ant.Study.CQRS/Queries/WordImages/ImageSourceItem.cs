﻿using System.Text.Json.Serialization;

namespace Ant.Study.CQRS.Queries.WordImages
{
	public class ImageSourceItem
	{
		[JsonPropertyName("i")]
		public long ID { get; set; }

		[JsonPropertyName("n")]
		public string Name { get; set; }

		[JsonPropertyName("pou")]
		public string PartOfUrl { get; set; }
	}
}
