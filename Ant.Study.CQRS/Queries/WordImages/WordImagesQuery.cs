﻿using Ant.Study.Common.Queries;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Ant.Study.CQRS.Queries.WordImages
{
	public class WordImagesQuery : IWordImagesQuery
	{
		Data _data = new Data();

		public void Request(IQueryArgs args)
		{
			var mediaSourceDic = new Dictionary<long, ImageSource>();
			var result = new List<ImageSource>();
			using var connection = args.ConnectionType.CreateConnection();
			using (var reader = _data.GetMediaSources(connection, args.UserID))
			{
				while (reader.Read())
				{
					var imageSource = new ImageSource()
					{
						ID = reader.GetValue<long>("ID"),
						Url = reader.GetValue<string>("Url")
					};
					imageSource.Items = new List<ImageSourceItem>();
					imageSource.IntersectionOfWords = new List<CrossWord>();
					mediaSourceDic.Add(imageSource.ID, imageSource);
				}
			}
			
			using (var reader = _data.GetMediaSourceItems(connection, args.UserID))
			{
				while(reader.Read())
				{
					var mediaResouice = mediaSourceDic[reader.GetValue<long>("MediaResourceID")];
					mediaResouice.Items.Add(new ImageSourceItem()
					{
						ID = reader.GetValue<long>("ID"),
						Name = reader.GetValue<string>("Name"),
						PartOfUrl = reader.GetValue<string>("PartOfUrl")
					});
				}
			}

			using(var reader = _data.GetIntersectionOfWords(connection, args.UserID))
			{
				while(reader.Read())
				{
					var mediaSource = mediaSourceDic[reader.GetValue<long>("MediaSourceID")];
					mediaSource.IntersectionOfWords.Add(new CrossWord()
					{
						MediaSourceItemID = reader.GetValue<long>("MediaSourceItemID"),
						WordID = reader.GetValue<long>("ID"),
						Name = reader.GetValue<string>("Name"),
					});
				}
			}
			args.ReturnModel = mediaSourceDic.Values.ToList();
		}
	}
}
