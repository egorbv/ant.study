﻿using Ant.Study.Common.Data;
using NpgsqlTypes;
using System.Data;

namespace Ant.Study.CQRS.Queries.WordImages
{
	internal class Data
	{
		internal IDataReader GetMediaSources(IStoreConnection connection, long? userID)
		{
			using var command = connection.NextCommand("SELECT * FROM public.\"WordImages_GetMediaSources\"(:pUserID)");
			command.AddParameter< NpgsqlDbType>("pUserID", NpgsqlDbType.Bigint, userID);
			return command.ExecuteReader();
		}

		internal IDataReader GetMediaSourceItems(IStoreConnection connection, long? userID)
		{
			using var command = connection.NextCommand("SELECT * FROM public.\"WordImages_GetMediaSourceItems\"(:pUserID)");
			command.AddParameter<NpgsqlDbType>("pUserID", NpgsqlDbType.Bigint, userID);
			return command.ExecuteReader();
		}

		internal IDataReader GetIntersectionOfWords(IStoreConnection connection, long? userID)
		{
			using var command = connection.NextCommand("SELECT * FROM public.\"WordImages_GetIntersectionOfWords\"(:pUserID)");
			command.AddParameter<NpgsqlDbType>("pUserID", NpgsqlDbType.Bigint, userID);
			return command.ExecuteReader();

		}
	}
}
