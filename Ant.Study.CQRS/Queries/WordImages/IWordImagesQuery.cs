﻿using Ant.Study.Common.Queries;

namespace Ant.Study.CQRS.Queries.WordImages
{
	[QueryWebApi(Name = "WordImagesQuery")]
	public interface IWordImagesQuery : IQuery
	{
	}
}
