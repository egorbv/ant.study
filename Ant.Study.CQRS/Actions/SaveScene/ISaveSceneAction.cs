﻿using Ant.Study.Common.Actions;

namespace Ant.Study.CQRS.Actions.SaveScene
{
	[ActionWebApi(Name = "SaveSceneAction", Type = typeof(Scene))]
	public interface ISaveSceneAction : IAction
	{
	}
}
