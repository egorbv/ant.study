﻿using Ant.Study.Common.Data;
using NpgsqlTypes;
using System;

namespace Ant.Study.CQRS.Actions.SaveScene
{
	internal class Data
	{
		internal void Save(IStoreConnection connection, Scene scene)
		{
			if (scene.ID.HasValue)
			{
				using var command = connection.NextCommand("SELECT public.\"Scene_Update\"(:pID, :pName, pData)");
				command.AddParameter<NpgsqlDbType>("pID", NpgsqlDbType.Bigint, scene.ID);
				command.AddParameter<NpgsqlDbType>("pName", NpgsqlDbType.Varchar, scene.Name);
				command.AddParameter<NpgsqlDbType>("pData", NpgsqlDbType.Varchar, scene.Data);
			}
			else
			{
				using var command = connection.NextCommand("SELECT * public.\"Scene_Update\"(:pID, :pName, pData)");
				command.AddParameter<NpgsqlDbType>("pID", NpgsqlDbType.Bigint, scene.ID);
				command.AddParameter<NpgsqlDbType>("pName", NpgsqlDbType.Varchar, scene.Name);
				command.AddParameter<NpgsqlDbType>("pData", NpgsqlDbType.Varchar, scene.Data);
				scene.ID = Convert.ToInt64(command.ExecuteScalar());
			}
		}
	}
}
