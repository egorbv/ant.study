﻿using Ant.Study.Common.Actions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ant.Study.CQRS.Actions.SaveScene
{
	public class SaveSceneAction : ISaveSceneAction
	{
		Data _data = new Data();
		
		public void Execute(IActionArgs args)
		{
			var scene = args.Model as Scene;
			if(scene == null)
			{
				throw new Exception("Передана не верная модель для действия");
			}
			if(string.IsNullOrEmpty(scene.Name))
			{
				throw new Exception("У сцены должно быть название");
			}
			using (var connection = args.ConnectionType.CreateConnection())
			{
				_data.Save(connection, scene);
			}
		}
	}
}
