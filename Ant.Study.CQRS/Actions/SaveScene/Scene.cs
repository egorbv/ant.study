﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ant.Study.CQRS.Actions.SaveScene
{
	public class Scene
	{
		public long? ID { get; set; }
		
		public string Name { get; set; }
		
		public string Data { get; set; }
	}
}
