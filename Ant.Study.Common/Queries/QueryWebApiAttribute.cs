﻿using System;

namespace Ant.Study.Common.Queries
{
	/// <summary>
	/// Атрибут указывает имя для запроса по web api
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface)]
	public class QueryWebApiAttribute : Attribute
	{
		/// <summary>
		/// Название запроса для web api
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Тип модели возвращаемую запросом
		/// </summary>
		public Type ReturnType { get; set; }

		/// <summary>
		/// Тип модели фильтра для запроса
		/// </summary>
		public Type FilterType { get; set; }

	}
}
