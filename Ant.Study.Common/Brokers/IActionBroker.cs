﻿using System;
using System.Collections.Generic;
using Ant.Study.Common.Actions;

namespace Ant.Study.Common.Brokers
{
	/// <summary>
	/// Интерфейс описывает брокера действий
	/// </summary>
	public interface IActionBroker
	{
		/// <summary>
		/// Зарегистрировать обаботчик для действия
		/// </summary>
		/// <typeparam name="T">Тип действия</typeparam>
		void Register<T>() where T : IAction;

		/// <summary>
		/// Выполнить обработчик действия
		/// </summary>
		/// <param name="webApyQueryName">имя запроса для webApi</param>
		/// <param name="args">Аргументы действия</param>
		void Execute(string webApyQueryName, IActionArgs args);

		/// <summary>
		/// Выполнить обработчик действия
		/// </summary>
		/// <typeparam name="T">Тип выполняемого действия</typeparam>
		/// <param name="args">Аргументы действия</param>
		void Execute<T>(IActionArgs args);

		/// <summary>
		/// Получить список всех зарегистрированных обработчиков действий и их моделей
		/// Tuple: item1 - название действия, item2 - обработчик действия, item3 - тип модели для запроса действия
		/// </summary>
		/// <returns>Cписок всех зарегистрированных обработчиков действий и их моделей</returns>
		//List<Tuple<string, Type, Type>> GetRegisteredWebApiActionTypes();
	}
}
