﻿using Ant.Study.Common.Data;
using Ant.Study.Common.Queries;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Ant.Study.Common.Brokers
{
	/// <summary>
	/// Брокер запросов
	/// </summary>
	public class QueryBroker : IQueryBroker
	{
		#region Fields
		/// <summary>
		/// Список обработчиков запросов
		/// </summary>
		Dictionary<Type, IQuery> _queries;

		/// <summary>
		/// Список обработчиков запросов для WebApi
		/// </summary>
		Dictionary<string, IQuery> _webApiQueries;

		/// <summary>
		/// Провайдер внедренных сервисов
		/// </summary>
		IServiceProvider _serviceProvider;

		/// <summary>
		/// Объект синхронизации
		/// </summary>
		object _sync;
		#endregion

		#region constructor
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="serviceProvider">Экземпляр провайдера внедренных сервисов</param>
		public QueryBroker(IServiceProvider serviceProvider)
		{
			_queries = new Dictionary<Type, IQuery>();
			_webApiQueries = new Dictionary<string, IQuery>();
			_sync = new object();
			_serviceProvider = serviceProvider;
		}
		#endregion

		#region Request
		/// <summary>
		/// Выполнение запроса
		/// </summary>
		/// <typeparam name="T">Тип необходимого запроса</typeparam>
		/// <param name="args">Аргументы запроса</param>
		public void Request<T>(IQueryArgs args)
		{
			var type = typeof(T);
			if(_queries.ContainsKey(type))
			{
				var query = _queries[type];
				args.ConnectionType = new StoreConnectionType();
				query.Request(args);
			}
		}

		/// <summary>
		/// Выполнение запроса
		/// </summary>
		/// <param name="webApyQueryName">Название web api query </typeparam>
		/// <param name="args">Аргументы запроса</param>
		public void Request(string webApyQueryName, IQueryArgs args)
		{
			if(_webApiQueries.ContainsKey(webApyQueryName))
			{
				args.ConnectionType = new StoreConnectionType();
				_webApiQueries[webApyQueryName].Request(args);
			}
		}
		#endregion

		#region Register
		/// <summary>
		/// Регистрация обработчика типа запроса
		/// </summary>
		/// <typeparam name="T">Тип запроса</typeparam>
		public void Register<T>() where T : IQuery
		{
			lock (_sync)
			{
				if (_queries.ContainsKey(typeof(T)))
				{
					throw new Exception($"Query {typeof(T)} allready registered");
				}

				var queryHandler = _serviceProvider.GetRequiredService<T>();
				_queries.Add(typeof(T), queryHandler);

				var queryWebApiAttribute = typeof(T).GetCustomAttributes(true)
					.SingleOrDefault(x => x.GetType() == typeof(QueryWebApiAttribute)) as QueryWebApiAttribute;
				if (queryWebApiAttribute != null)
				{
					if(_webApiQueries.ContainsKey(queryWebApiAttribute.Name))
					{
						throw new Exception($"Web Api Query {typeof(T)} allready registered");
					}
					_webApiQueries.Add(queryWebApiAttribute.Name, queryHandler);
				}
			}
		}
		#endregion

		//#region GetRegisteredWebApiQueryTypes
		///// <summary>
		///// Получить список всех зарегистрированных обработчиков действий и их моделей.
		///// Tuple: item1 - название запроса, item2 - обработчик запроса, item3 - тип модели фильтра для запроса
		///// </summary>
		///// <returns>Cписок всех зарегистрированных обработчиков действий и их моделей</returns>
		//public List<Tuple<string, Type,Type>> GetRegisteredWebApiQueryTypes()
		//{
		//	var count = _queries.Count;
		//	var registeredQueries = new List<Tuple<string, Type, Type>>();
		//	foreach (var item in _webApiQueries)
		//	{
		//		var queryFilterModel = item.Value.GetType().GetCustomAttributes(true)
		//			.SingleOrDefault(x => x.GetType() == typeof(QueryFilterModelAttribute)) as QueryFilterModelAttribute;
				
		//		if (queryFilterModel != null)
		//		{
		//			registeredQueries.Add(new Tuple<string, Type, Type> ( item.Key, item.Value.GetType(), queryFilterModel.Type));
		//		}
		//		else
		//		{
		//			registeredQueries.Add(new Tuple<string, Type, Type>(item.Key, item.Value.GetType(), null));
		//		}
		//	}
		//	return registeredQueries;
		//}
		//#endregion
	}
}
