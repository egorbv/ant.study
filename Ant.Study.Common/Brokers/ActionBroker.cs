﻿using Ant.Study.Common.Actions;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Ant.Study.Common.Data;
using System.Linq;
using System.Text.Json;

namespace Ant.Study.Common.Brokers
{
	/// <summary>
	/// Брокер действий
	/// </summary>
	public class ActionBroker : IActionBroker
	{
		#region Fields
		/// <summary>
		/// Словарь зарегистрированных действий
		/// </summary>
		Dictionary<Type, IAction> _actions;

		/// <summary>
		/// Список обработчиков дейсьвий для web api
		/// </summary>
		Dictionary<string, (IAction Action, Type Type)> _webApiActions;

		/// <summary>
		/// Экземпляр провайдера сервисов
		/// </summary>
		IServiceProvider _serviceProvider;

		/// <summary>
		/// Объект синхронизации
		/// </summary>
		object _sync;
		#endregion

		#region constructor
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="serviceProvider">Экземпляр провайдера сервисов</param>
		public ActionBroker(IServiceProvider serviceProvider)
		{
			_actions = new Dictionary<Type, IAction>();
			_webApiActions = new Dictionary<string, (IAction Action, Type Type)>();
			_sync = new object();
			_serviceProvider = serviceProvider;
		}
		#endregion

		#region Execute
		/// <summary>
		/// Выполнить обработчик действия
		/// </summary>
		/// <typeparam name="T">Тип выполняемого действия</typeparam>
		/// <param name="args">Аргументы действия</param>
		public void Execute<T>(IActionArgs args)
		{
			var type = typeof(T);
			if (_actions.ContainsKey(type))
			{
				var action = _actions[type];
				args.ConnectionType = new StoreConnectionType();
				action.Execute(args);
			}
		}


		/// <summary>
		/// Выполнить обработчик действия
		/// </summary>
		/// <param name="webApiQueryName">Название действия для web api</param>
		/// <param name="args">Аргументы действия</param>
		public void Execute(string webApiQueryName, IActionArgs args)
		{
			if (_webApiActions.ContainsKey(webApiQueryName))
			{
				var actionInfo = _webApiActions[webApiQueryName];
				args.ConnectionType = new StoreConnectionType();
				if(actionInfo.Type != null)
				{
					args.Model = JsonSerializer.Deserialize(args.Model.ToString(), actionInfo.Type, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
				}
				actionInfo.Action.Execute(args);
			}
		}
		#endregion

		#region Register
		/// <summary>
		/// Зарегистрировать обаботчик для действия
		/// </summary>
		/// <typeparam name="T">Тип действия</typeparam>
		public void Register<T>() where T : IAction
		{
			lock (_sync)
			{
				if (_actions.ContainsKey(typeof(T)))
				{
					throw new Exception($"Action {typeof(T)} allready trgistered");
				}
				var actionHandler = _serviceProvider.GetRequiredService<T>();
				if(actionHandler == null)
				{
					throw new Exception($"Не найден сервис для действия {typeof(T).FullName}");
				}
				_actions.Add(typeof(T), actionHandler);

				var actionWebApiAttribute = typeof(T).GetCustomAttributes(false)
					.SingleOrDefault(x => x.GetType() == typeof(ActionWebApiAttribute)) as ActionWebApiAttribute;

				if (actionWebApiAttribute != null)
				{
					if (_webApiActions.ContainsKey(actionWebApiAttribute.Name))
					{
						throw new Exception($"Web Api Action {typeof(T)} allready registered");
					}
					_webApiActions.Add(actionWebApiAttribute.Name, (actionHandler, actionWebApiAttribute.Type));
				}
				else
				{
					throw new Exception($"Для действия не {typeof(T).FullName} не указан атрибут ActionWebApiAttribute");
				}
			}
		}
		#endregion
	}
}
