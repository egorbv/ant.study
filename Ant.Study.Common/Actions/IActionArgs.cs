﻿using Ant.Study.Common.Data;

namespace Ant.Study.Common.Actions
{
	/// <summary>
	/// Интерфейс описывает базовый аргумент действия
	/// </summary>
	public interface IActionArgs
	{
		/// <summary>
		/// Идентификатор действия
		/// </summary>
		int UserID { get; set; }

		/// <summary>
		/// Тип соеденения к БД
		/// </summary>
		IStoreConnectionType ConnectionType { get; set; }

		/// <summary>
		/// Экземпляр возвращаемой модели
		/// </summary>
		object ReturnedModel { get; set; }

		/// <summary>
		/// Экземпляр модели для действия
		/// </summary>
		object Model { get; set; }

		/// <summary>
		/// Пользовательская метка для действия
		/// </summary>
		string UserTag { get; set; }
	}
}
