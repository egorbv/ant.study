﻿using System;

namespace Ant.Study.Common.Actions
{
	/// <summary>
	/// Атрибут указывает название для действия web api
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface)]
	public class ActionWebApiAttribute : Attribute
	{
		/// <summary>
		/// Название действия web api
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Обрабатываемый действием тип модели 
		/// </summary>
		public Type Type { get; set; }
	}
}
