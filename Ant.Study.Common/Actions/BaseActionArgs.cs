﻿using Ant.Study.Common.Data;

namespace Ant.Study.Common.Actions
{
	/// <summary>
	/// Базовый аргумент для действия
	/// </summary>
	public class BaseActionArgs : IActionArgs
	{
		/// <summary>
		/// Идентификатор пользователя
		/// </summary>
		public int UserID { get; set; }

		/// <summary>
		/// Тип соеденения к БД
		/// </summary>
		public IStoreConnectionType ConnectionType { get; set; }

		/// <summary>
		/// Экземпляр возвращаемой модели
		/// </summary>
		public object ReturnedModel { get; set; }

		/// <summary>
		/// Экземпляр модели для действия
		/// </summary>
		public object Model { get; set; }

		/// <summary>
		/// Пользовательская метка для действия
		/// </summary>
		public string UserTag { get; set; }
	}
}
