﻿namespace Ant.Study.Common.Actions
{
	/// <summary>
	/// Интерфейс описывает базовый экшен
	/// </summary>
	public interface IAction
	{
		/// <summary>
		/// Выполнить экшен
		/// </summary>
		/// <param name="args">Аргумент экшена</param>
		void Execute(IActionArgs args);

	}
}
