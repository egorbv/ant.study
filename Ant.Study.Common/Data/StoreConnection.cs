using Npgsql;
using NpgsqlTypes;
using System.Data;
using System.Diagnostics;

namespace Ant.Study.Common.Data
{
	internal class StoreConnection : IStoreConnection
	{
		object _transactionOwner;
		object _rootOwner;

        NpgsqlTransaction _transaction;
        NpgsqlConnection _connection;

		public IStoreConnection CreateConnection()
		{
			var storeConnection = new StoreConnection(_rootOwner, _connection);
			storeConnection._transactionOwner = _transactionOwner;
			storeConnection._transaction = _transaction;
			return storeConnection;
		}


		internal StoreConnection(object rootOwner, NpgsqlConnection connection)
		{
			if (rootOwner != null)
			{
				_rootOwner = rootOwner;
			}
			else
			{
				_rootOwner = this;
			}
			_connection = connection;
		}

		public IDbCommand NextCommand(string commandName)
		{
			var command = new NpgsqlCommand(commandName, _connection);
			command.Transaction = _transaction;
			command.CommandType = CommandType.Text;
			command.CommandText = commandName;
			if (_connection.State != ConnectionState.Open)
			{
				_connection.Open();
			}
			return command;
		}

		public void BeginTransaction()
		{
			if (_transactionOwner == null)
			{
				if (_connection.State != ConnectionState.Open)
				{
					_connection.Open();
				}
				_transactionOwner = this;
				_transaction = _connection.BeginTransaction();
			}
		}

		public void RollbackTransaction()
		{
			if (_transactionOwner == this)
			{
				_transactionOwner = null;
				_transaction.Rollback();
				_transaction = null;
			}
		}

		public void CommitTransaction()
		{
			if (_transactionOwner == this)
			{
				_transactionOwner = null;
				_transaction.Commit();
				_transaction = null;
			}
		}

		public void Dispose()
		{
			if (_transactionOwner == this)
			{
				_transaction.Rollback();
			}

			if (_rootOwner == this)
			{
				Debug.WriteLine("Disposed");
				if (_connection.State == ConnectionState.Open)
				{
					_connection.Close();
					_connection.Dispose();
				}
			}
		}
	}
}
