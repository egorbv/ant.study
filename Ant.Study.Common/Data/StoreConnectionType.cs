using Npgsql;

namespace Ant.Study.Common.Data
{
	public class StoreConnectionType : IStoreConnectionType
	{
		IStoreConnection _storeConnection;
        NpgsqlConnection _connection;
		static string _defaultConnectionString;

		public StoreConnectionType()
		{
			
			_connection = new NpgsqlConnection(_defaultConnectionString);
		}

		public StoreConnectionType(string connectionString)
		{
			_connection = new NpgsqlConnection(connectionString);
		}

		public static void SetDefaultConnectionString(string connectionString)
		{
			_defaultConnectionString = connectionString;
		}

		public IStoreConnection CreateConnection()
		{
			if (_storeConnection == null)
			{
				_storeConnection = new StoreConnection(null, _connection);
				return _storeConnection;
			}
			else
			{
				return _storeConnection.CreateConnection();
			}
		}
	}
}
