using NpgsqlTypes;
using System;
using System.Data;

namespace Ant.Study.Common.Data
{
	public static class SqlCommandHelper
	{
        public static void AddParameter<T>(this IDbCommand command, string name, T type, object value)
        {
            if (type.GetType() == typeof(NpgsqlDbType))
            {
                var t = (NpgsqlDbType)(type as object);
                if (value == null)
                {
                    command.Parameters.Add(new Npgsql.NpgsqlParameter(name, DBNull.Value) { NpgsqlDbType = t });

                }
                else
                {
                    command.Parameters.Add(new Npgsql.NpgsqlParameter(name, value) { NpgsqlDbType = t });
                }
            }
        }
    }
}
