using System;
using System.Data;

namespace Ant.Study.Common.Data
{
	/// <summary>
	/// Интерфейс описывает тип подключения к источнику данных
	/// </summary>
	public interface IStoreConnection : IDisposable, IStoreConnectionType
	{
		/// <summary>
		/// Начало транзакции
		/// </summary>
		/// <param name="owner">Экземпляр класса владельца транзакции</param>
		/// <returns></returns>
		void BeginTransaction();


		/// <summary>
		/// Комит текущей транзакции
		/// </summary>
		/// <param name="owner">Владелец транзакции</param>
		void CommitTransaction();

		/// <summary>
		/// Получения экземпляра класса команды к источнику данных
		/// </summary>
		/// <param name="commandName"></param>
		/// <returns></returns>
		IDbCommand NextCommand(string commandName);

		/// <summary>
		/// Откат транзакции
		/// </summary>
		void RollbackTransaction();
	}
}
