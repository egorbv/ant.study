import { Component, Inject } from '@angular/core';
import { IActionService } from '../services/action-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  
  dragableTextData: IDragableTextData =  {};
  dragableImage: any = {};
  mainCopntainer: HTMLElement;
  currentTabIndex = 1;

  constructor(@Inject('IActionService') private actionService: IActionService) {
  }

  public ngOnInit() {
    this.mainCopntainer = document.getElementById("scene-editor");
  }

  saveScene() {
    let data = new Array<string>();
    this.mainCopntainer.childNodes.forEach((x: HTMLElement) => {
      let childType = "";
      switch (x.className) {
        case "scene-editor-text":
          childType = "T";
          break;
        case "scene-editor-image-container":
          childType = "I";
          break;
      }
      data.push(`${childType},${x.offsetLeft},${x.offsetTop}|${x.innerHTML}\u0000`);
    })
    this.actionService.saveScene({ name: 'ffff', data: data.join() }).subscribe(x => {
      debugger;
    });
  }

  draggableMove(e: KeyboardEvent) {
    let el: HTMLElement = document.activeElement as HTMLElement;
    if (e.code == "NumpadDecimal" || e.code == "Backspace") {
      el.parentNode.removeChild(el);
    }
    if (e.code == "ArrowDown") {
      el.style.top = (el.offsetTop + 1) + "px";
    }
    if (e.code == "ArrowUp") {
      el.style.top = (el.offsetTop - 1) + "px";
    }
    if (e.code == "ArrowLeft") {
      el.style.left = (el.offsetLeft - 1) + "px";
    }
    if (e.code == "ArrowRight") {
      el.style.left = (el.offsetLeft + 1) + "px";
    }
  }

  pastText() {
    let text = document.createElement("div");
    text.draggable = true;
    text.className = "scene-editor-text";
    text.innerHTML = "Какой то текст";
    text.onblur = this.draggableTextBlur;
    this.pastObject(text);
  }

  pastImage(image: any) {
    let div = document.createElement("div");
    div.className = "scene-editor-image-container";
    div.draggable = true;
    let img = document.createElement("img");
    img.src = image.src;
    img.draggable = false;
    div.appendChild(img);
    this.pastObject(div);
  }

  pastObject(node: HTMLElement) {
    node.tabIndex = this.currentTabIndex;
    this.currentTabIndex++;
    this.mainCopntainer.appendChild(node);
    let left = this.mainCopntainer.offsetWidth / 2 - node.offsetWidth / 2 + this.mainCopntainer.scrollLeft;
    let top = this.mainCopntainer.offsetHeight / 2 - node.offsetHeight / 2 + this.mainCopntainer.scrollTop;
    node.style.left = left + "px";
    node.style.top = top + "px";
  }

  draggableTextBlur(e) {
    e.target.contentEditable = false;
    e.target.style.cursor = "grab";
    //нужно востановить для корректного перетаскивания
    e.target.style.position = "absolute";
  }

  draggableTextDoubleClick(e) {
    e.target.contentEditable = true;
    e.target.style.cursor = "text";
    //нужно, что бы в тексте при редактировании вставлялись BR вместо DIV
    e.target.style.position = "relative";
    e.target.focus();
  }

  draggableStart(e) {
    this.dragableImage = null;
    this.dragableTextData = null;
    if (e.srcElement.className == "scene-editor-image-container") {
    } else {
      e.target.contentEditable = false;
    }
    this.dragableImage = { Node: e.target, OffsetX: e.offsetX, OffsetY: e.offsetY };
    e.dataTransfer.setData("text/html", e.target.outerHTML);
  }

  draggableDrop(e) {
    if (this.dragableImage != null) {
      this.setOffsetElement(e, this.dragableImage);
    }
    else
    {
      this.setOffsetElement(e, this.dragableTextData);
    }
    event.preventDefault();
  }

  setOffsetElement(event, node) {
    let offsetY = event.offsetY;
    let offsetX = event.offsetX;
    let nodeTarget = event.target;
    while (nodeTarget.className != "scene-editor") {
      offsetX += nodeTarget.offsetLeft;
      offsetY += nodeTarget.offsetTop
      nodeTarget = nodeTarget.parentNode;
    }
    node.Node.style.left = (offsetX - node.OffsetX ) + "px";
    node.Node.style.top = (offsetY - node.OffsetY) + "px";
  }
}

interface IDragableTextData {
  Node?: HTMLElement;
  OffsetX?: number;
  OffsetY?: number;
}  

interface ImageSource {
  url: string;
  images: ImageInfo[];
}

interface ImageInfo {
  name: string;
  partOfUrl: string;
}
