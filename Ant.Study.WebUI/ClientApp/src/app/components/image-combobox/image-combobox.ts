import { Inject, Component, Output, EventEmitter } from '@angular/core';
import { IQueryService } from '../../services/query-service';
import { IMediaSource, IMediaSourceItem } from '../../services/query-service-types';


@Component({
  selector: 'image-combobox',
  templateUrl: './image-combobox.html',
})

export class ImageCombobox {
  @Output() onImageInsert = new EventEmitter(); 

  filteredImages: Array<IImageComboboxInfo>;
  filterImageTimerHandler: any;
  filterPatternValue = "";
  filterListOpened = false;
  mediaSources: Map<number, IMediaSource>;
  mediaSourceItems: Map<number, IMediaSourceItem>;

  constructor(@Inject('IQueryService') private queryService: IQueryService) {
  }

  ngOnInit() {
    this.queryService.getMediaSources().subscribe(data => {
      this.mediaSources = new Map<number, IMediaSource>();
      this.mediaSourceItems = new Map<number, IMediaSourceItem>();
      data.forEach(mediaSource => {
        this.mediaSources.set(mediaSource.id, mediaSource)
        mediaSource.items.forEach(item => {
          this.mediaSourceItems.set(item.id, item);
        })
        mediaSource.intersectionOfWords.forEach(word => {
          word.name = word.name.toLowerCase();
        })
      });
    });
  }

  get filterPattern(){
    return this.filterPatternValue;
  }

  filterImageChanged(text: string) {
    this.filterPatternValue = text;
    clearTimeout(this.filterImageTimerHandler);
    this.filterImageTimerHandler = setTimeout(x => {
      this.filteredImages = new Array<IImageComboboxInfo>();
      if (text.trim() !== "") {
        for (let [mediaSourceID, mediaSourceItem] of this.mediaSources) {
          for (let crossWord of mediaSourceItem.intersectionOfWords) {
            if (crossWord.name.substr(0, text.length) == text) {
              this.filteredImages.push({ url: mediaSourceItem.url + this.mediaSourceItems.get(crossWord.mediaSourceItemID).partOfUrl, name: crossWord.name });
            }
          }
        }
      }
    }, 500);
  }

  filterImageOpenList() {
    this.filterListOpened = true;
    document.body.addEventListener("mousedown", x => { this.bodyMouseUpHandler(this) });
  }

  bodyMouseUpHandler(x) {
    let ev: Event = event || window.event;
    if ((ev.target as Element).closest(".image-combobox") == null) {
      this.filterListOpened = false;
    } else {
      document.body.removeEventListener("mousedown", this.bodyMouseUpHandler);
    }
  }

  pastImage(image) {
    this.filteredImages = null;
    this.filterPatternValue = "";
    this.onImageInsert.emit(image);
    document.body.removeEventListener("mousedown", this.bodyMouseUpHandler);
  }
}

interface IImageComboboxInfo {
  url: string;
  name: string;
}

