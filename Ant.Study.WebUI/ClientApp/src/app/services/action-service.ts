import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface IActionService {
  saveScene(IScene): Observable<void>;
}

@Injectable()
export class ActionService implements IActionService {

  constructor(private http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
  }

  saveScene(scene: IScene): Observable<void>{
    var content = new HttpParams();
    content = content.set("data", JSON.stringify(scene));
    return this.http.post(this.baseUrl + "/Api/Action?actionName=SaveSceneAction", content)
      .pipe(map(data => {
        scene.id = 0;
      }));
  }
}


export interface IScene {
  id?: number;
  name: string;
  data: string;
}


