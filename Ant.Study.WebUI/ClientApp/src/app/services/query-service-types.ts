export interface IMediaSource {
  id: number;
  url: string;
  items: Array<IMediaSourceItem>;
  intersectionOfWords: Array<ICrossWord>;
}

export interface IMediaSourceItem {
  id: number;
  name: string;
  partOfUrl: string;
}

export interface ICrossWord {
  mediaSourceItemID: number;
  wordID: number;
  name: string;
}
