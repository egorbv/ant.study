import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IMediaSource, IMediaSourceItem, ICrossWord } from './query-service-types';

export interface IQueryService {
  getMediaSources(): Observable<IMediaSource[]>;
}

@Injectable()
export class QueryService implements IQueryService {

  constructor(private http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
  }

  getMediaSources(): Observable<IMediaSource[]> {
    return this.http.get<IMediaSource[]>(this.baseUrl + "/Api/Query?queryName=WordImagesQuery").pipe(map(data => {
      let result = new Array<IMediaSource>();
      let obj: any = data;
     
      for (var mc of obj) {
        let mediaSource: IMediaSource = {id: mc.i, url: mc.u, items: new Array<IMediaSourceItem>(), intersectionOfWords: new Array<ICrossWord>() };
        result.push(mediaSource);
        for (let item of mc.it) {
          mediaSource.items.push({ id: item.i, name: item.n, partOfUrl : item.pou });
        }
        for (let word of mc.iow) {
          mediaSource.intersectionOfWords.push({ mediaSourceItemID: word.msd, wordID: word.wi, name : word.n });
        }
      }
      return result;
    }));
  }
}


