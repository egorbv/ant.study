import { Text } from '../input/text.js'
import { Date } from '../input/date.js'
import { Week } from '../input/week.js'
import { Time } from '../input/time.js'
import { Month } from '../input/month.js'
import { Checkbox } from '../input/check-box.js'
import { Range } from '../input/range.js'
import { Color } from '../input/color.js'
import { Url } from '../input/url.js'
import { Table } from '../table/table.js'
import { Toolbox } from '../toolbox/toolbox.js'

export class DocumentEditor {

    constructor(container) {
        this.container = container;
        this.components = new Array();        
    }

    Init() {
        let html = "";
        html += '<div class="ant-tool-panel"></div>';
        html += '<div class="ant-center-panel" align=center><div contenteditable="true"><br/></div></div>'
        this.container.innerHTML = html;
        let tollboxContainer = document.getElementsByClassName("ant-tool-panel")[0];
        let editorContainer = document.getElementsByClassName("ant-center-panel")[0];

        this.mutationObserver = new MutationObserver((x, y) => this.mutationObserverHandler(x, y))
        this.mutationObserver.observe(editorContainer.childNodes[0], { childList: true, subtree: true });

        this.components.push(new Text());
        this.components.push(new Date());
        this.components.push(new Month());
        this.components.push(new Week());
        this.components.push(new Time());
        this.components.push(new Checkbox());
        this.components.push(new Range());
        this.components.push(new Color());
        this.components.push(new Url());
        this.components.push(new Table());

        this.toolbox = new Toolbox(tollboxContainer);
        this.toolbox.Init(this.components);

        this._identifierManager = new IdentifierManager();
    }

    mutationObserverHandler(mutationsList, observer) {
        this.components.forEach(item => {
            for (let i = 0; i < mutationsList.length; i++) {
                if (item.BindElements != undefined) {
                    item.BindElements(mutationsList[i].addedNodes, mutationsList[i].removedNodes, this._identifierManager);
                }
            }
        })
    }
}

export class IdentifierManager {
    _identifiers = [];
    _currentElementIndex = 0;

    Exists(id) {
        return this._identifiers[id] != undefined;
    }

    GenerateID() {
        this._currentElementIndex++;
        return this._currentElementIndex;
    }

    Add(id, data) {
        if (this._identifiers[id] != undefined) {
            throw new Error("Элемент с таким идентификатором уже существуюет");
        }
        this._identifiers[id] = data;
    }

    Delete(id) {

    }
}

