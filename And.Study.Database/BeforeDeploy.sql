﻿select pg_terminate_backend (pg_stat_activity.pid) from pg_stat_activity where pg_stat_activity.datname = 'Study';
DROP DATABASE IF EXISTS "Study";

CREATE DATABASE "Study";
SET search_path = "Study";

DROP ROLE IF EXISTS study;
CREATE ROLE study LOGIN PASSWORD '12qw34ER';

GRANT CREATE ON DATABASE "Study" TO study;