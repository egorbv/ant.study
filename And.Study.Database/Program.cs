﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace And.Study.Database
{
	class Program
	{
		static void Main(string[] args)
		{
			var path = Environment.CurrentDirectory;
			var binIndex = path.ToLower().IndexOf("\\bin");
			if(binIndex > 0)
			{
				path = path.Substring(0, binIndex);
			}
			var builder = new ConfigurationBuilder()
				.SetBasePath(path)
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
			var configuration = builder.Build();


			Console.WriteLine("Start recreate database");
			var dbCreator = new DbCreator(configuration, path);
			Console.WriteLine("Database was created");
			Console.ReadKey();
		}
	}
}
