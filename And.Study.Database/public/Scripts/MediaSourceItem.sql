﻿INSERT INTO public."MediaSourceItem" ("MediaSourceID", "ID", "Name", "PartOfUrl") VALUES (1, 1, 'Бабушка и дедушка', 'abuelo_abuela.png');
INSERT INTO public."MediaSourceItem" ("MediaSourceID", "ID", "Name", "PartOfUrl") VALUES (1, 2, 'Бабушка и дедушка', 'abuelo_abuela1.png');
INSERT INTO public."MediaSourceItem" ("MediaSourceID", "ID", "Name", "PartOfUrl") VALUES (1, 3, 'Бабушка и дедушка', 'abuelo_abuela2.png');
INSERT INTO public."MediaSourceItem" ("MediaSourceID", "ID", "Name", "PartOfUrl") VALUES (1, 4, 'Бабушка и дедушка', 'abuelo_abuela3.png');

INSERT INTO public."MediaSourceItem" ("MediaSourceID", "ID", "Name", "PartOfUrl") VALUES (1, 5, 'Бабушка', 'abuela1.png');
INSERT INTO public."MediaSourceItem" ("MediaSourceID", "ID", "Name", "PartOfUrl") VALUES (1, 6, 'Дедушка', 'abuelo1.png');
INSERT INTO public."MediaSourceItem" ("MediaSourceID", "ID", "Name", "PartOfUrl") VALUES (1, 7, 'Семья', 'familia1.png');


ALTER SEQUENCE public."MediaSourceItem_ID_seq" RESTART WITH 1000;
