﻿CREATE TABLE public."MediaSource"
(
    "ID" BIGSERIAL NOT NULL,
    "Url" VARCHAR(1024) NOT NULL,
    "UserID" BIGINT NULL,
    "Description" VARCHAR(2048) NULL,
    CONSTRAINT "MediaSource_pkey" PRIMARY KEY ("ID")
);

