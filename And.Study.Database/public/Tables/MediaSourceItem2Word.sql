﻿CREATE TABLE public."MediaSourceItem2Word"
(
    "MediaSourceItemID" BIGINT NOT NULL,
    "WordID" BIGINT NOT NULL,
    CONSTRAINT "MediaSourceItem2Word_pkey" PRIMARY KEY ("MediaSourceItemID", "WordID")
);

##ForeignKey
ALTER TABLE public."MediaSourceItem2Word" 
    ADD CONSTRAINT "MediaSourceItem2Word_MediaSourceID" FOREIGN KEY ("MediaSourceItemID") REFERENCES public."MediaSourceItem" ("ID"); 
ALTER TABLE public."MediaSourceItem2Word" 
    ADD CONSTRAINT "MediaSourceItem2Word_WordID" FOREIGN KEY ("WordID") REFERENCES public."Word" ("ID"); 
