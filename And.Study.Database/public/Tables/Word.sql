﻿CREATE TABLE public."Word"
(
    "ID" BIGSERIAL NOT NULL,
    "Name" VARCHAR(64),
    CONSTRAINT "Word_pkey" PRIMARY KEY ("ID")
);
