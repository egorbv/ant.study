﻿CREATE TABLE public."Scene"
(
    "ID" BIGSERIAL NOT NULL,
    "UserID" BIGINT NOT NULL,
    "Name" VARCHAR(128) NOT NULL,
    "Data" VARCHAR NULL,
    CONSTRAINT "Scene_pkey" PRIMARY KEY ("ID")
);

