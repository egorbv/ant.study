CREATE TABLE public."MediaSourceItem"
(
    "MediaSourceID" BIGINT NOT NULL,
    "ID" BIGSERIAL NOT NULL,
    "Name" VARCHAR(64),
    "PartOfUrl" VARCHAR(1024),
    CONSTRAINT "MediaSourceItem_pkey" PRIMARY KEY ("MediaSourceID", "ID"),
    CONSTRAINT "MediaSourceItem_id" UNIQUE ("ID")
);

##ForeignKey
ALTER TABLE public."MediaSourceItem" 
    ADD CONSTRAINT "MediaSourceItem_MediaSourceID" FOREIGN KEY ("MediaSourceID") REFERENCES public."MediaSource" ("ID"); 