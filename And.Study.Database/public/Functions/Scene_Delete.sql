﻿CREATE OR REPLACE FUNCTION public."Scene_Delete"("pID" BIGINT)
RETURNS VOID AS 
$$
BEGIN
	DELETE FROM public."Scene"
		WHERE
			"ID" = "pID";
END
$$
LANGUAGE  plpgsql;