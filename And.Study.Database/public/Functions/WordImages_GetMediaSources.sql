CREATE OR REPLACE FUNCTION public."WordImages_GetMediaSources"("pUserID" BIGINT)
RETURNS TABLE
(
    "ID" BIGINT,
    "Url" VARCHAR
) AS
$$
BEGIN
    RETURN QUERY
    SELECT ms."ID", ms."Url" 
        FROM 
            public."MediaSource" ms
        WHERE
            ms."UserID" IS NULL OR ("pUserID" IS NOT NULL AND "pUserID" = ms."UserID");
END $$
LANGUAGE  plpgsql;