﻿CREATE OR REPLACE FUNCTION public."WordImages_GetMediaSourceItems"("pUserID" BIGINT)
RETURNS TABLE
(
    "MediaResourceID" BIGINT,
    "ID" BIGINT,
    "Name" VARCHAR,
    "PartOfUrl" VARCHAR
) AS
$$
BEGIN
    RETURN QUERY
    SELECT msi."MediaSourceID", msi."ID", msi."Name", msi."PartOfUrl"
        FROM 
            public."MediaSource" ms
            INNER JOIN public."MediaSourceItem" msi ON msi."MediaSourceID" = ms."ID"
        WHERE
            ms."UserID" IS NULL OR ("pUserID" IS NOT NULL AND "pUserID" = ms."UserID");

END $$
LANGUAGE  plpgsql;