﻿CREATE OR REPLACE FUNCTION public."Scene_Insert"("pUserID" BIGINT, "pName" VARCHAR(128), "pData" VARCHAR)
RETURNS BIGINT AS
$$
DECLARE "pID" BIGINT; 
BEGIN
    INSERT INTO public."Scene"
            ("UserID", "Name", "Data")
        VALUES("pUserID", "pName", "pData") RETURNING "ID" INTO "pID";
    RETURN "pID";
END $$
LANGUAGE  plpgsql;