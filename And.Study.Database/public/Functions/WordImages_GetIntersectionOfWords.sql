﻿CREATE OR REPLACE FUNCTION public."WordImages_GetIntersectionOfWords"("pUserID" BIGINT)
RETURNS TABLE
(
    "MediaSourceID" BIGINT,
    "MediaSourceItemID" BIGINT,
    "ID" BIGINT,
    "Name" VARCHAR
) AS
$$
BEGIN
    RETURN QUERY
    SELECT  msi."MediaSourceID", msi."ID" As MediaSourceItemID, w."ID", w."Name"
	    FROM
		    public."MediaSource" ms
		    INNER JOIN public."MediaSourceItem" msi ON msi."MediaSourceID" = ms."ID"
		    INNER JOIN public."MediaSourceItem2Word" msiw ON msiw."MediaSourceItemID" = msi."ID"
		    INNER JOIN public."Word" w ON w."ID" = msiw."WordID"
        WHERE
            ms."UserID" IS NULL OR ("pUserID" IS NOT NULL AND "pUserID" = ms."UserID");

END $$
LANGUAGE  plpgsql;