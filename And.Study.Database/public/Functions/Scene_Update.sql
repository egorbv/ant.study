﻿CREATE OR REPLACE FUNCTION public."Scene_Update"("pID" BIGINT, "pUserID" BIGINT, "pName" VARCHAR(128), "pData" VARCHAR)
RETURNS VOID AS 
$$
BEGIN
	UPDATE public."Scene"
		SET
			"Name" = "pName",
			"Data" = "pData"
		WHERE
			"ID" = "pID";
END
$$
LANGUAGE  plpgsql;