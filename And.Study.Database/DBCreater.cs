using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;

namespace And.Study
{
    public class DbCreator
	{
		List<string> _foreignKeys;
		NpgsqlConnection _connection;

		public DbCreator(IConfiguration configuration, string rootFolder)
		{
			var adminConnectionString = configuration["Postgresql:AdminConnectionString"];
			var userConnectionString = configuration["Postgresql:UserConnectionString"];

			_foreignKeys = new List<string>();
			using (_connection = new NpgsqlConnection(adminConnectionString))
			{
				_connection.Open();
				try
				{

					Console.WriteLine("Пересоздание базы данных");
					var fileName = rootFolder + "/BeforeDeploy.sql";
					if (File.Exists(fileName))
					{
						var script = File.ReadAllText(fileName);
						_executeScript(script, _connection);
					}

					_connection.Close();

					_connection.ConnectionString = userConnectionString;
					_connection.Open();



					Console.WriteLine("Создание схем");
					fileName = rootFolder + "/Schema.sql";
					if (File.Exists(fileName))
					{
						var script = File.ReadAllText(fileName);
						_executeScript(script, _connection);
					}

					var schems = Directory.GetDirectories(rootFolder);
					Console.WriteLine("Создание таблиц");
					for (var i = 0; i < schems.Length; i++)
					{
						_createSchemaTables(schems[i] + "/Tables");
					}

					Console.WriteLine("Создание функций");
					for (var i = 0; i < schems.Length; i++)
					{
						_createSchemaFunction(schems[i] + "/Functions");
					}

					Console.WriteLine("Выполнение скриптов");
					for (var i = 0; i < schems.Length; i++)
					{
						_executeSchemaScripts(schems[i] + "/Scripts");
					}

					for(var i=0; i < _foreignKeys.Count; i++)
					{
						_executeScript(_foreignKeys[i], _connection);
					}
				}
				finally
				{
					if (_connection.State == System.Data.ConnectionState.Open)
					{
						_connection.Close();
					}
				}
			}
		}


		void _createSchemaTables(string tableFolder)
		{
			if (Directory.Exists(tableFolder))
			{
				var scripts = Directory.GetFiles(tableFolder);
				for (var i = 0; i < scripts.Length; i++)
				{
					var fileName = scripts[i];
					var tmp = File.ReadAllText(fileName).Split("##ForeignKey");
					_executeScript(tmp[0], _connection);
					if (tmp.Length == 2)
					{
						_foreignKeys.Add(tmp[1]);
					}
				}
			}
		}

		void _createSchemaFunction(string functionFolder)
		{
			if (Directory.Exists(functionFolder))
			{
				var scripts = Directory.GetFiles(functionFolder);
				for (var i = 0; i < scripts.Length; i++)
				{
					var fileName = scripts[i];
					var script = File.ReadAllText(fileName);
					_executeScript(script, _connection);
				}
			}
		}

		void _executeSchemaScripts(string scriptFolder)
		{
			if (Directory.Exists(scriptFolder))
			{
				var scripts = Directory.GetFiles(scriptFolder);
				for (var i = 0; i < scripts.Length; i++)
				{
					var fileName = scripts[i];
					var script = File.ReadAllText(fileName);
					_executeScript(script, _connection);
				}
			}
		}

		void _executeScript(string script, NpgsqlConnection connection)
		{
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = script;
                    command.ExecuteNonQuery();
                }
            }
            catch(Exception e)
            {
				Console.WriteLine(e.Message);
				throw;
            }
		}
	}
}